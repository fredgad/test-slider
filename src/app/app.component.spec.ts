import { ComponentFixture, TestBed } from '@angular/core/testing';
import { AppComponent } from './app.component';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { SliderModule } from '../slider/feature/slider.module';
import { ApiService } from '../slider/entity/services/api.service';
import { SliderSettingsI } from '../slider/entity/interfaces/slider.interface';
import { sliderMockData } from '../slider/entity/mock/slider.mock';
import { of } from 'rxjs';

describe('AppComponent', () => {
  let fixture: ComponentFixture<AppComponent>;
  let component: AppComponent;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [HttpClientTestingModule, RouterTestingModule, SliderModule],
      declarations: [AppComponent],
      providers: [ApiService],
    }).compileComponents();

    fixture = TestBed.createComponent(AppComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('Should initialize slides from ApiService', () => {
    const apiService = TestBed.inject(ApiService);
    const slides: SliderSettingsI[] = sliderMockData;
    jest.spyOn(apiService, 'getSlider').mockReturnValue(of(slides));

    expect(component.slides$).toBeTruthy();
    component.slides$.subscribe((data) => {
      expect(data).toEqual(slides);
    });
  });
});
