import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { AppComponent } from './app.component';
import { SliderModule } from '../slider/feature/slider.module';
import { HttpClientModule } from '@angular/common/http';
import { RouterModule } from '@angular/router';
import { ApiService } from '../slider/entity/services/api.service';

@NgModule({
  declarations: [AppComponent],
  imports: [
    BrowserModule,
    HttpClientModule,
    SliderModule,
    RouterModule.forRoot([]),
  ],
  providers: [ApiService],
  bootstrap: [AppComponent],
})
export class AppModule {}
