import { Component, OnInit, inject } from '@angular/core';
import { ApiService } from '../slider/entity/services/api.service';
import { SliderSettingsI } from '../slider/entity/interfaces/slider.interface';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
})
export class AppComponent implements OnInit {
  private apiService = inject(ApiService);

  public slides$: Observable<SliderSettingsI[]> = this.apiService.slider$;

  public ngOnInit(): void {
    this.apiService.getSlider();
  }
}
