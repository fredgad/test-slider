import { ComponentFixture, TestBed } from '@angular/core/testing';
import { SliderComponent } from './slider.component';
import { SliderQueryParamsService } from '../entity/services/slider-query-params.service';
import { of, Subject } from 'rxjs';
import { ActivatedRoute } from '@angular/router';
import { sliderMockData } from '../entity/mock/slider.mock';
import { RouterTestingModule } from '@angular/router/testing';
import { HttpClientTestingModule } from '@angular/common/http/testing';
const mockRoute = {
  queryParamMap: of({ get: (sliderName: string) => '2' }),
};
describe('SliderComponent', () => {
  let component: SliderComponent;
  let fixture: ComponentFixture<SliderComponent>;
  let queryParamsService: SliderQueryParamsService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule, RouterTestingModule],
      declarations: [SliderComponent],
      providers: [
        SliderQueryParamsService,
        {
          provide: ActivatedRoute,
          useValue: mockRoute,
        },
      ],
    });

    fixture = TestBed.createComponent(SliderComponent);
    component = fixture.componentInstance;
    queryParamsService = TestBed.inject(SliderQueryParamsService);

    fixture.detectChanges();
  });

  it('Should initialize slider', () => {
    jest
      .spyOn(queryParamsService, 'getSliderIndexByName')
      .mockReturnValue(of(0));

    component.ngOnInit();
    component.ngAfterViewInit();

    expect(component.slideWidth).toBe(0);
    expect(queryParamsService.getSliderIndexByName).toHaveBeenCalled();
  });

  it('Should go to the next slide', () => {
    component.currentSlide = 0;
    const slidesToShow = 1;
    component.slidesToShow = slidesToShow;
    component.sliderLenght = 3;

    component.next();

    expect(component.currentSlide).toBe(slidesToShow);
  });
});
