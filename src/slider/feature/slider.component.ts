import {
  AfterViewInit,
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  ElementRef,
  Input,
  OnDestroy,
  OnInit,
  ViewChild,
} from '@angular/core';
import {
  BehaviorSubject,
  Observable,
  Subject,
  Subscription,
  combineLatest,
  filter,
  interval,
} from 'rxjs';
import { SliderSettingsI } from '../entity/interfaces/slider.interface';
import { SliderQueryParamsService } from '../entity/services/slider-query-params.service';

@Component({
  selector: 'app-slider',
  templateUrl: './slider.component.html',
  styleUrls: ['./slider.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class SliderComponent implements OnInit, AfterViewInit, OnDestroy {
  @ViewChild('slider') public slider!: ElementRef<HTMLElement>;

  @Input() slides$: Observable<SliderSettingsI[]> = new BehaviorSubject([]);
  @Input() slidesToShow: number = 1;
  @Input() slidesToScroll: number = 1;
  @Input() autoplaySpeed: number = 5000;
  @Input() sliderName: string = 'slider_1';

  private subscriptions: Subscription[] = [];

  public hostWidth$: Subject<number> = new Subject();

  public slideWidth: number = 0;
  public currentSlide: number = 0;
  public currentPosition: number = 0;
  public sliderLenght: number = 0;

  constructor(
    private elementRef: ElementRef<HTMLElement>,
    private sliderParamsService: SliderQueryParamsService,
    private cdr: ChangeDetectorRef
  ) {}

  public ngOnInit(): void {
    this.initSlider();
  }

  public ngAfterViewInit(): void {
    if (this.slider) {
      this.hostWidth$.next(this.elementRef.nativeElement.offsetWidth);
    }
  }

  public next(): void {
    this.currentSlide =
      this.currentSlide > this.sliderLenght - (this.slidesToScroll + 1)
        ? 0
        : this.currentSlide + this.slidesToScroll;

    this.setSlide(this.currentSlide);
  }

  private setSlide(slideNumber: number): void {
    this.sliderParamsService.setCurrentSlideByName(
      slideNumber,
      this.sliderName
    );
    this.currentPosition = -(slideNumber * this.slideWidth);
  }

  private initSlider(): void {
    this.subscriptions = [
      this.sliderParamsService
        .getSliderIndexByName(this.sliderName)
        .pipe(filter((paramsData) => paramsData !== 0))
        .subscribe((paramsData) => {
          this.currentSlide = paramsData;
          this.setSlide(paramsData);
          this.cdr.markForCheck();
        }),

      combineLatest([this.slides$, this.hostWidth$]).subscribe(
        ([slides, hostWidth]) => {
          this.sliderLenght = slides.length;
          this.slideWidth = hostWidth / this.slidesToShow;
          this.cdr.detectChanges();
        }
      ),
    ];

    this.startAutoPlay();
  }

  private startAutoPlay(): void {
    interval(this.autoplaySpeed).subscribe(() => {
      this.next();
      this.cdr.markForCheck();
    });
  }

  public trackBySlide(_index: number, slide: any): any {
    return slide.id;
  }

  public ngOnDestroy(): void {
    this.subscriptions.forEach((s: Subscription) => s?.unsubscribe());
  }
}
