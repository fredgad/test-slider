export interface SliderSettingsI {
  id: number; // - идентификатор слайда
  image: string; // - URL картинки слайда
  url: string; // - URL для перехода при нажатии на слайд
  priority: number; // - приоритет показа слайда
}
