import { TestBed, getTestBed } from '@angular/core/testing';
import { SliderQueryParamsService } from './slider-query-params.service';
import { Router, ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';

describe('SliderQueryParamsService', () => {
  let injector: TestBed;
  let sliderService: SliderQueryParamsService;
  let router: Router;
  let route: ActivatedRoute;

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [
        SliderQueryParamsService,
        {
          provide: Router,
          useValue: {
            navigate: jest.fn(),
          },
        },
        {
          provide: ActivatedRoute,
          useValue: {
            queryParamMap: of({
              get: (param: string) => {
                if (param === 'slider_1') {
                  return '2';
                }
                return null;
              },
            }),
          },
        },
      ],
    });

    injector = getTestBed();
    sliderService = injector.inject(SliderQueryParamsService);
    router = injector.inject(Router);
    route = injector.inject(ActivatedRoute);
  });

  it('should set and get current slide index by name', () => {
    const sliderName = 'slider_1';
    sliderService.setCurrentSlideByName(3, sliderName);
    sliderService.getSliderIndexByName(sliderName).subscribe((index) => {
      expect(index).toBe(3);
    });

    expect(router.navigate).toHaveBeenCalledWith([], {
      relativeTo: route,
      queryParams: { [sliderName]: 3 },
      queryParamsHandling: 'merge',
    });
  });
});
