import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import {
  BehaviorSubject,
  Observable,
  Subject,
  catchError,
  map,
  of,
} from 'rxjs';
import { SliderSettingsI } from '../interfaces/slider.interface';
import { sliderMockData } from '../mock/slider.mock';

@Injectable({
  providedIn: 'root',
})
export class ApiService {
  private apiUrl = 'https://some/api/slider';
  public slider$: Observable<SliderSettingsI[]> = this.getSlider();

  constructor(private http: HttpClient) {}

  public getSlider(): Observable<SliderSettingsI[]> {
    const params = new HttpParams({
      fromObject: {
        token: 'TOKEN',
      },
    });

    return of(sliderMockData).pipe(
      map((arr) => {
        return arr.sort((a, b) => b.priority - a.priority);
      })
    );

    return this.http.get<SliderSettingsI[]>(`${this.apiUrl}`, { params }).pipe(
      map((arr) => {
        return arr.sort((a, b) => b.priority - a.priority);
      })
    );
  }
}
