import { Injectable } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { BehaviorSubject, Observable, Subject, map, of } from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export class SliderQueryParamsService {
  private currentSlideIndex$ = new Subject<any>();

  constructor(private router: Router, private route: ActivatedRoute) {}

  public setCurrentSlideByName(index: number, sliderName: string): void {
    this.currentSlideIndex$.next(index);
    this.updateQueryParamByName(index, sliderName);
  }

  public getSliderIndexByName(sliderName: string): Observable<number> {
    return this.route.queryParamMap.pipe(
      map((params: any) => {
        const slide = params.get(sliderName);
        const slideNumber = slide ? Number(slide) : 0;
        return slideNumber;
      })
    );
  }

  private updateQueryParamByName(index: number, sliderName: string): void {
    this.router.navigate([], {
      relativeTo: this.route,
      queryParams: { [sliderName]: index },
      queryParamsHandling: 'merge',
    });
  }
}
