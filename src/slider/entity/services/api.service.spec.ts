import { TestBed, getTestBed } from '@angular/core/testing';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { ApiService } from './api.service';
import { SliderSettingsI } from '../interfaces/slider.interface';
import { sliderMockData } from '../mock/slider.mock';

describe('ApiService', () => {
  let injector: TestBed;
  let apiService: ApiService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [ApiService],
    });

    injector = getTestBed();
    apiService = injector.inject(ApiService);
  });

  it('Should return sorted slider data', () => {
    apiService.slider$.subscribe((sliderData: SliderSettingsI[]) => {
      expect(sliderData).toEqual(sliderMockData);
      for (let i = 0; i < sliderData.length - 1; i++) {
        expect(sliderData[i].priority).toBeLessThanOrEqual(
          sliderData[i + 1].priority
        );
      }
    });
  });
});
