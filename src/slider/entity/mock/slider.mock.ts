import { SliderSettingsI } from '../interfaces/slider.interface';

export const sliderMockData: SliderSettingsI[] = [
  {
    id: 0,
    image:
      'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQs6wEf-NNJ7xgdz-fW8oIg81snkoomof9L9pP4QCS3Sw&s',
    url: 'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQs6wEf-NNJ7xgdz-fW8oIg81snkoomof9L9pP4QCS3Sw&s',
    priority: 3,
  },
  {
    id: 1,
    image:
      'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSyl7Tugwo5XQH5nUwZqOPgoLCtYQwq_9MOfdrUAK7r0A&s',
    url: 'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSyl7Tugwo5XQH5nUwZqOPgoLCtYQwq_9MOfdrUAK7r0A&s',
    priority: 1,
  },
  {
    id: 2,
    image: 'https://androidinsider.ru/wp-content/uploads/2018/09/1_Lenovo.jpg',
    url: 'https://androidinsider.ru/wp-content/uploads/2018/09/1_Lenovo.jpg',
    priority: 2,
  },
  {
    id: 3,
    image:
      'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcT8GTKVA7u24CYNx7NCf0KBv_aS9XCyMK_V4xGYI7B8Cw&s',
    url: 'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcT8GTKVA7u24CYNx7NCf0KBv_aS9XCyMK_V4xGYI7B8Cw&s',
    priority: 7,
  },
  {
    id: 4,
    image:
      'https://smartslider3.com/wp-content/uploads/2019/05/sliderimages-780x410.png',
    url: 'https://smartslider3.com/wp-content/uploads/2019/05/sliderimages-780x410.png',
    priority: 8,
  },
  {
    id: 5,
    image:
      'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQI3EipmMy-o0PIjrEgAYwgwBp0-5Mh0QUiBchXAmE8&s',
    url: 'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQI3EipmMy-o0PIjrEgAYwgwBp0-5Mh0QUiBchXAmE8&s',
    priority: 5,
  },
  {
    id: 6,
    image: 'https://musicsale.com.ua/images/blog/159/guitar-slider.jpg',
    url: 'https://musicsale.com.ua/images/blog/159/guitar-slider.jpg',
    priority: 6,
  },
  {
    id: 7,
    image:
      'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRSIh2qANWsYpRL1wB-lPbAlu0BSa0oS51BIw&usqp=CAU',
    url: 'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRSIh2qANWsYpRL1wB-lPbAlu0BSa0oS51BIw&usqp=CAU',
    priority: 4,
  },
];
